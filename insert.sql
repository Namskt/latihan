INSERT INTO public.users(id, username, password, role_id, permission_id, created_at)
VALUES
  ('550e8400-e29b-41d4-a716-446655440000', 'anam1', 'anam123', '7acf58a3-4a86-4574-b3c1-d1a5175ad480', '6a7a0fe3-080d-4190-9af7-8b9f332e4c47', 2023-12-04 21:47:06.740092),
  ('123e4567-e89b-12d3-a456-426614174001', 'anam2', 'anam123', '7acf58a3-4a86-4574-b3c1-d1a5175ad480', 'd2b690b4-f7b9-4b26-83a8-9e1e9a6b2a8d', 2023-12-04 21:47:06.740092);
  
INSERT INTO public.role(id, name, created_at)
VALUES
  ('7acf58a3-4a86-4574-b3c1-d1a5175ad480', 'Admin', 2023-12-04 21:46:59.844322),
  ('8ccf58a3-4a86-4574-b3c1-d1a5175ad480', 'User', 2023-12-04 21:46:59.844322);
  
INSERT INTO public.permissions(id, name, created_at)
VALUES
  ('6a7a0fe3-080d-4190-9af7-8b9f332e4c47', 'ReadAdmin', 2023-12-04 21:46:59.844322),
  ('d2b690b4-f7b9-4b26-83a8-9e1e9a6b2a8d', 'WriteUser', 2023-12-04 21:46:59.844322);
